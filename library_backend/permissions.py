from rest_framework.permissions import BasePermission
from django.db.models import Q

class IsStaff(BasePermission):
	def has_permission(self,request,view):
		user=request.user.__class__.__name__
		if user.lower()=='anonymoususer':
			return False
		try:
			user_role=request.user.role.name
		except:
			user_role = None
		if user_role and user_role.lower() == "staff":
			return True
		return False

class IsCustomer(BasePermission):
	def has_permission(self,request,view):
		user=request.user.__class__.__name__
		if user.lower()=='anonymoususer':
			return False
		try:
			user_role=request.user.role.name
		except:
			user_role = None
		if user_role and user_role.lower() == "customer":
			return True
		return False