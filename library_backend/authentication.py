import jwt
from django.conf import settings
from rest_framework import authentication, exceptions
from usertype.models import Users


class JWTUserAuthentication(authentication.BaseAuthentication):
	authentication_header_prefix = 'Bearer'
	def authenticate(self, request):
		request.user = None
		
		auth_header = authentication.get_authorization_header(request).split()
		auth_header_prefix = self.authentication_header_prefix.lower()

		if not auth_header:
			return None
		if len(auth_header) == 1:
			return None
		elif len(auth_header) > 2:
			return None
		
		prefix = auth_header[0].decode('utf-8')
		token = auth_header[1].decode('utf-8')

		if prefix.lower() != auth_header_prefix:
			return None
		return self._authenticate_credentials(request, token)
	
	def _authenticate_credentials(self, request, token):

		if token:
			try:
				payload = jwt.decode(token, settings.SECRET_KEY,algorithms=['HS256'])
			except:
				msg = 'Invalid authentication. Could not decode token.'
				raise exceptions.AuthenticationFailed(msg)	
			try:
				user = Users.objects.get(pk=payload.get('id'))
			except Users.DoesNotExist:
				return None
 
		if not user:
			msg = 'This user has been deactivated.'
			raise exceptions.AuthenticationFailed(msg)
		return (user, None)

