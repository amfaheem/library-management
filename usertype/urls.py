from rest_framework import routers
from usertype.views import  UserViewset


router = routers.SimpleRouter()
router.register(r'users', UserViewset, basename='users')
urlpatterns=[]
urlpatterns += router.urls