from django.db import models
from shared.models import BaseModel


class Role(BaseModel):
	name = models.CharField(max_length=150,unique=True) #Staff,Customer,SuperAdmin
	is_active = models.BooleanField(default=True)
	class Meta:
		db_table = 'roles'

class Users(BaseModel):
	name = models.CharField(max_length=50)
	phone_number = models.CharField(max_length= 15, unique=True, db_index=True)
	phn_country_code = models.CharField(max_length= 5, null=True)
	email = models.CharField(max_length=50,unique=True,db_index=True)
	password = models.CharField(max_length=250)
	role = models.ForeignKey(Role,on_delete=models.PROTECT)
	class Meta:
		db_table = 'users'
		constraints = [ models.UniqueConstraint(fields=['phone_number','email','phn_country_code'], name='unique_user')]
	
	def generate_token(self):
		import jwt, datetime, time
		from django.conf import settings
		dt = datetime.datetime.now() + datetime.timedelta(days=15)
		token = jwt.encode({
			'user_type': 'staff',
			'id': self.id,
			'exp': int(time.mktime(dt.timetuple()))
			}, settings.SECRET_KEY, algorithm='HS256')
		return token



