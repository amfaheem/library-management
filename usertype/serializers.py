from rest_framework import serializers
from usertype.models import Users
from django.contrib.auth.hashers import make_password,check_password


class UsersSerializer(serializers.ModelSerializer):
	role=serializers.SerializerMethodField()
	class Meta:
		model = Users
		exclude = ('created_at','updated_at','password')
	
	def get_role(self,obj):
		return obj.role.name if obj.role else  None
		

class SignupSerializer(serializers.ModelSerializer):
	class Meta:
		model = Users
		exclude = ('created_at','updated_at')
	
	def to_internal_value(self, data):
		data["password"]=make_password(data["password"])
		return super().to_internal_value(data)
	


class LoginSerializer(serializers.Serializer):
	email = serializers.CharField(required=True)
	password = serializers.CharField(required=True)

	def validate(self, data):
		errors = []
		if not data.get("email"):
			errors.append("Email is required")
		if not data.get("password"):
			errors.append("Password is required")
		qs = Users.objects.filter(email=data.get("email"))
		if not qs:
			errors.append("No user found with following credentials")
		else:
			current_password = qs.first().password
			request_password = data.get("password")			
			check_passwrd=check_password(request_password,current_password)
			if check_passwrd == False:
				errors.append("Password not match with following credentials")
		if len(errors):
			raise serializers.ValidationError(errors)
		return super().validate(data)