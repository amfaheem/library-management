from rest_framework import viewsets
from usertype.models import Role, Users
from rest_framework.decorators import action
from usertype.serializers import LoginSerializer, SignupSerializer, UsersSerializer
from rest_framework.response import Response


class UserViewset(viewsets.ModelViewSet):
    queryset = Users.objects.all()
    serializer_class = UsersSerializer
    authentication_classes=[]
    permission_classes = []
    http_method_names = ['get', 'post']

    @action(detail=False, methods=['POST'],authentication_classes=[])
    def staff_signup(self,request):
        requested_data = request.data
        role_name="staff"
        role = Role.objects.filter(name=role_name).first()
        if not role:
            role=Role.objects.create(name=role_name)
        requested_data['role']=role.id
        serializer=SignupSerializer(data=requested_data)
        if serializer.is_valid():
            serializer.save()
            return Response({"message":"User created successfully"},status=200)
        return Response(serializer.errors)
    
    @action(detail=False, methods=['POST'],authentication_classes=[])
    def customer_signup(self,request):
        requested_data = request.data
        role_name="customer"
        role = Role.objects.filter(name=role_name).first()
        if not role:
            role=Role.objects.create(name=role_name)
        requested_data['role']=role.id
        serializer=SignupSerializer(data=requested_data)
        if serializer.is_valid():
            serializer.save()
            return Response({"message":"User created successfully"},status=200)
        return Response(serializer.errors)

    @action(detail=False, methods=['POST'],authentication_classes=[])
    def login(self,request):
        requested_data = request.data
        seriailizer = LoginSerializer(data=requested_data)
        if seriailizer.is_valid():
            data=seriailizer.data
            data.pop("password")
            user=Users.objects.filter(email=data.get("email")).first()
            if user:
                return Response({"token":user.generate_token(),**UsersSerializer(user).data})
        return Response(seriailizer.errors)
    