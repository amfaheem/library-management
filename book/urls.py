from rest_framework import routers
from book.views import  BookViewset


router = routers.SimpleRouter()
router.register(r'book', BookViewset, basename='book')
urlpatterns=[]
urlpatterns += router.urls