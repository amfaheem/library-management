# Generated by Django 3.2.19 on 2023-06-13 17:43

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import shared.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('usertype', '0002_auto_20230613_1450'),
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('updated_at', shared.models.AutoDateTimeField(default=django.utils.timezone.now)),
                ('name', models.CharField(max_length=150)),
            ],
            options={
                'db_table': 'author',
            },
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('updated_at', shared.models.AutoDateTimeField(default=django.utils.timezone.now)),
                ('name', models.CharField(max_length=150)),
                ('isbn', models.CharField(max_length=50)),
                ('is_published', models.BooleanField(default=True)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='all_books', to='book.author')),
                ('created_user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='created_books', to='usertype.users')),
            ],
            options={
                'db_table': 'book',
            },
        ),
        migrations.CreateModel(
            name='Publisher',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('updated_at', shared.models.AutoDateTimeField(default=django.utils.timezone.now)),
                ('name', models.CharField(max_length=150)),
            ],
            options={
                'db_table': 'publisher',
            },
        ),
        migrations.CreateModel(
            name='ReadRequest',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('updated_at', shared.models.AutoDateTimeField(default=django.utils.timezone.now)),
                ('start_time', models.DateTimeField()),
                ('end_time', models.DateTimeField()),
                ('is_request_approved', models.BooleanField(default=False)),
                ('approved_staff', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='approved_request', to='usertype.users')),
                ('book', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='requested_books', to='book.book')),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='my_request', to='usertype.users')),
            ],
            options={
                'db_table': 'readrequest',
            },
        ),
        migrations.AddField(
            model_name='book',
            name='published_by',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='published_books', to='book.publisher'),
        ),
    ]
