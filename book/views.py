from rest_framework import viewsets
from rest_framework.response import Response
from book.models import Book, ReadRequest
from book.serializers import BookCreateSerializer, BookSerializer, ReadRequestValidator
from library_backend.authentication import JWTUserAuthentication
from library_backend.permissions import IsCustomer, IsStaff
from rest_framework.decorators import action


class BookViewset(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    authentication_classes=[JWTUserAuthentication]
    permission_classes = [IsStaff]
    http_method_names = ['get', 'post']

    def get_permissions(self):
        if self.action == 'create' or self.action == 'update' or self.action == 'partial_update' or self.action == "approve_request":
            return [IsStaff()]
        if self.action =="read_request":
            return [IsCustomer()]
        return [IsStaff(),IsCustomer()]
    
    def get_serializer_class(self):
        if self.action == "create":
            return BookCreateSerializer
        return BookSerializer
    
    @action(methods=['POST'], detail=False, authentication_classes=[JWTUserAuthentication],permission_classes=[IsCustomer])
    def read_request(self,request):
        requested_data= request.data
        if not requested_data:
            return Response({"message":"Payload not found"},status=400)
        if not requested_data.get('book'):
            return Response({"message":"Book id not found"},status=400)
        bk_qs=Book.objects.filter(id=requested_data['book'])
        if not bk_qs:
            return Response({"message":"No book found"},status=400)
        readqs=ReadRequest()
        readqs.customer=request.user
        readqs.book=bk_qs.first()
        readqs.save()
        return Response({"message":f"Your request for {readqs.book.name} is success..please wait for confirmation"},status=200)
        
    @action(methods=['POST'], detail=False, authentication_classes=[JWTUserAuthentication],permission_classes=[IsStaff])
    def approve_request(self,request):
        requested_data= request.data
        serializer=ReadRequestValidator(data=requested_data)
        if not requested_data:
            return Response({"message":"Payload not found"},status=400)
        if not requested_data.get('id'):
            return Response({"message":"Request id not found"},status=400)
        read_request=ReadRequest.objects.filter(id=requested_data['id']).first()
        if read_request.is_request_approved:
            return Response({"message":"Request is already approved"},status=400)
        if not read_request:
            return Response({"message":"No Request found with given details"},status=400)
        if serializer.is_valid():
            read_request.approved_staff = request.user
            read_request.start_time = requested_data.get('start_time')
            read_request.end_time = requested_data.get('end_time')
            read_request.is_request_approved=True
            read_request.save()
            return Response({"message":f"Request for {read_request.book.name} by {read_request.customer.name} is approved"},status=200)
        return Response({"message":serializer.errors},status=400)


        
        


        

