from rest_framework import serializers
from book.models import Author, Book, Publisher, ReadRequest


class BookSerializer(serializers.ModelSerializer):
	class Meta:
		model = Book
		exclude = ('created_at','updated_at')

class BookCreateSerializer(serializers.ModelSerializer):
	class Meta:
		model = Book
		exclude = ('created_at','updated_at')
	
	def to_internal_value(self, data):
		data['created_user'] = self.context['request'].user.id
		author_data = data.pop('authors', None)
		published_by_data = data.pop('published_bys', None)
		if author_data:
			author, author_exist = Author.objects.get_or_create(**author_data)
			data["author"] = author.id
		if published_by_data:
			publisher, publisher_exist = Publisher.objects.get_or_create(**published_by_data)
			data["published_by"] = publisher.id
		return super().to_internal_value(data)

	def validate(self, attrs):
		bookqs=Book.objects.filter(name=attrs['name'],author=attrs['author'],published_by=attrs["published_by"])
		if bookqs:
			raise serializers.ValidationError("Book already exist")
		return super().validate(attrs)


class ReadRequestValidator(serializers.ModelSerializer):
	start_time = serializers.DateTimeField(required=True)
	end_time = serializers.DateTimeField(required=True)
	class Meta:
		model = ReadRequest
		fields = ('id','start_time','end_time')
	
	def validate(self, data):
		start_time = data.get('start_time')
		end_time = data.get('end_time')

		if start_time and end_time and start_time > end_time:
			raise serializers.ValidationError("Start time cannot be greater than end time.")

		return data