from django.db import models
from shared.models import BaseModel
from usertype.models import Users

class Author(BaseModel):
    name = models.CharField(max_length=150)
    class Meta:
        db_table = 'author'

class Publisher(BaseModel):
    name = models.CharField(max_length=150)
    class Meta:
        db_table = 'publisher'

class Book(BaseModel):
    name = models.CharField(max_length=150)
    isbn = models.CharField(max_length=50)
    author = models.ForeignKey(Author,on_delete=models.PROTECT,related_name='all_books',blank=True,null=True)
    published_by = models.ForeignKey(Publisher,on_delete=models.PROTECT,related_name='published_books',blank=True,null=True)
    created_user = models.ForeignKey(Users,on_delete=models.PROTECT,related_name='created_books',blank=True,null=True)
    is_published = models.BooleanField(default=True)
    class Meta:
        db_table = 'book'

class ReadRequest(BaseModel):
    customer  = models.ForeignKey(Users,on_delete=models.PROTECT,related_name='my_request')
    book  = models.ForeignKey(Book,on_delete=models.PROTECT,related_name='requested_books')
    approved_staff = models.ForeignKey(Users,on_delete=models.PROTECT,related_name='approved_request',blank=True,null=True)
    start_time = models.DateTimeField(blank=True,null=True)
    end_time = models.DateTimeField(blank=True,null=True)
    is_request_approved = models.BooleanField(default=False)
    class Meta:
        db_table = 'readrequest'